class FundRecordForm2sController < ApplicationController
  before_filter :get_resident
  before_filter :get_admin, only: [:new, :create, :show]

  def index
    @fund_record_form2s = @resident.fund_record_form2s
  end

  def new
    @fund_record_form2 = @resident.fund_record_form2s.build
    @admin = Employee.where(is_admin: true).first
    # @employee = current_employee
    @fund_transaction = @fund_record_form2.fund_transactions.build
  end

  def create
    @fund_record_form2 = FundRecordForm2.new(params[:fund_record_form2])
    @fund_record_form2.resident = Resident.find(params[:resident_id])
    @admin = Employee.where(is_admin: true).first
    # @fund_record_form2.employee = Employee.find(params[:employee_id])
    # @fund_transaction.FundRecordForm2.find(params[:])

    @fund_transaction = FundTransaction.new(params[:fund_transaction])

    # resident = Resindent.find(params[:resident_id])
    # current_user.residents.find(params[:resident_id])
    # resident.fund_record_form2s.build(params[:fund_record_form2])
    # resident.fund_record_form2s << FundRecordForm2.new(params[:fund_record_form2])
    #@resident.fund_record_form2s.build(params[:fund_record_form2])
    #@fund_transaction = @fund_record_form2.build(params[:fund_transactions])
    if @fund_record_form2.save
      flash[:success] = "Fund Record Form II created!"
      redirect_to [@resident, @fund_record_form2] 
    else
      flash.now[:error] = "There was a problem with the form"
      render :new 
    end



    #user
    #contacts

    # user.contacts

    # current_user.contacts.find(params[:id])

    #               Contact.where(user_id: current_user.id).where(id: params[:id]).first
  end

  def show
    # @employee = current_employee.find(params[:id])
    @fund_record_form2 = @resident.fund_record_form2s.find(params[:id])
    @fund_transaction = FundTransaction.find(params[:id])
  end

  def update
    @fund_record_form2 = @resident.fund_record_form2s.find(params[:id])
    if @fund_record_form2.update_attributes(params[:fund_record_form2])
      flash[:success] = "Form updated"
      redirect_to [@resident, @fund_record_form2], id: params[:id]
    else
      flash.now[:error] = "Unable to update form"
      render :edit
    end
  end

  def edit
    @fund_record_form2 = @resident.fund_record_form2s.find(params[:id])
  end

  def destroy
    @fund_record_form2 = @resident.fund_record_form2s.find(params[:id])
    @fund_record_form2.delete
    flash.now[:notice] = "You sure?"
    redirect_to resident_fund_record_form2s_path
  end

  private
  def get_resident
    @resident = Resident.find(params[:resident_id])
  end

  def get_admin 
    @admin = Employee.where(is_admin: true).first
  end
end
