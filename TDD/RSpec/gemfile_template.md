RSpec Gemfile Template
======================
___


**Add these to your Gemfile**
-----------------------------
```
group :development, :test do
	gem 'rspec-rails'
	gem 'factory_girl_rails'
end

group :test do 
	gem 'faker'
	gem 'database_cleaner'
	gem 'launchy'
	gem 'selenium-webdriver'
end
```

type ```bundle install``` which downloads the above dependencies

**Create ```:test``` & ```:development``` databases** 
-----------------------------------------------------  

type <code>bundle exec db:create:all</code> to create your databases in all environments

**RSpec config**
----------------  

```Ruby
	type ```bundle exec rails generate rspec:install```
	open ```.rspec` file & add `--format documentation``` to the file
```
**Generators**
--------------

Loading ```rspec-rails``` and ```factory_girl_rails``` will prevent Rails stock generators from creating 
```TEST::UNIT``` fiiles.

We can manually specify settings for our generators like so:

	1. open `config/application.rb'
	2. add the following code

```Ruby
config.generators do |g|
	g.test_framework :rspec, 
	  fixtures: true,
	  view_specs: false,
	  helper_specs: false, # consider changing to true in the future
	  routing_specs: false, # consider changing to true in the future
		controller_specs: true,
		request_specs: false
	g.fixture_replacement facory_girl, dir: "spec/factories"
end
```
**Test db Schema**
------------------

Even though we have a test database, it doesn't have a schema. 

	1. type ```rake db:test:clone``` to have ```:test``` schema match ```:development```

Although this clones the ```:development``` database structure into ```:test```, it doesn't copy any data.

**Extras: Create a Rails App Template**
---------------------------------------

Automatically add Gems, app config files and auto create a test database by using the following.

Checkout:
	1. [Rails Wizard on Github](https://github.com/intridea/rails_wizard)
	2. [App Scrolls](https://github.com/drnic/appscrolls)
