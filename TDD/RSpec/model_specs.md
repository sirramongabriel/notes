#Model Specs
------------

###RSpec Model Spec Outline
---------------------------  

```Ruby
describe Contact do
	it 'is valid with a firstname, lastname and email address'
	it 'is invalid without a firstname'
	it 'is invalide without a lastname'
	it 'is invalide without an email address'
	it 'is invalide with a duplicate email address'
	it 'returns a contacts fullname as a string'
end
```  

**The outline above points to four best practices:**  
	1. It describes a set of expectations 
		-In this case, what the contact model should look like
	2. Each example only expects one thing
		- In this case, we're testing firstname, lastname and email validations separately
		- If an example fails, we know it's because of that specific validation
		- We won't have to dig through Rspec's output for clues, at least not as deeply
	3. Each example is explicit
		- The descriptive string after 'it' is technically optional in Rspec
		- However, ommitting it makes our specs more difficult to read
	4. Each example's description begins with a verb, not should
		- Read the expectations aloud, the sentence should make sense
		- Readability is important

###Creating a Model Spec
------------------------  

**New Rspec Syntax:** 
In June 2012 Rspec announced an alternative convention to `should`. Instead of saying something `should` or `should_not` match expected output, we `expect()` something `to` or `not_to` `be` something else.

```Ruby
Ex: Old Syntax
it 'is true when true' do
	true.should be_true
end

Ex: New Syntax
it 'is true when true' do
	expect(true).to be_true
end
```

**Testing Validations**
Ex: Expect a new contact with `nil` firstname not to be valid.
```Ruby
it 'is invalid without a #firstname' do
	expect(Contact.new(firstname: nil)).to have(1).errors_on(:firstname)
end
```

**Flip the expectation to prove that you're not receiving false positives.
Ex: Flipping expectations
```Ruby
it 'is invalid without a #firstname' do
	expect(Contact.new(firstname: nil)).not_to have(1).errors_on(:firstname)
end
```

This is an easy way to verify your tests are working correctly.

Ex: Test for unique email address
```Ruby
it 'is invalid with a duplicate email address' do
	Contact.create(firstname: 'Joe', lastname: 'Tester', email: 'test@example.com')
	Contact.new(firstname: 'Jane, lastname: 'Tester', email: 'test@example.com')
	expect(contact).to have(2).errors_on(:email)
end
```
**Create test data & tell Rspec how you expect it to behave
Ex: Testing class methods and scopes
```Ruby
def self.by_letter(letter)
	where("firstname LIKE ?", "#{letter}%").order(:lastname)
end
```  
Ex: Class method spec
```Ruby
it 'returns a sorted array of results that match' do
	smith = Contact.create(firstname: 'John', lastname: 'Smith', email: 'jsmith@example.com')
	jones = Contact.create(firstname: 'Tim', lastname: 'Jones', email: 'jjones@example.com')
	johnson = Contact.create(firstname: 'John', lastname: 'Jones', email: 'jone@example.com')
	expect(Contact.by_letter('J')).to eq [Johnson, Jones]
end
```

###DRYer specs with describe, context, before & after
-----------------------------------------------------

**describe:**  Outlines the general functionality of a class
**context:**  Outlines a specific state (i.e. valid/invalid)
**before:**  Vital to cleaning up redundancy from specs. The code in this block runs `before` each example within the 
described block-but not outside of that block.

Since the 3 contacts in the above example are now in a before filter, we have to assign them to instance variables so that they're accessible outside the `before` block within our examples.

If a spec requires some sort of post-example teardown/disconnecting from an external service, we can use an `after` hook to cleanup after the examples. Since Rspec handles cleaning up the database by default, `after` is rarely used.

###How DRY is too DRY?
----------------------

When setting up test conditions for your example, it's OK to bend the DRY principle in the interest of readability.

If you find yourself scrolling up and down a large `spec` file to see what it is that you're testing(or loading too many external support files to your tests), consider duplicating your test data setup within smaller `describe` blocks or even within examples themselves.


